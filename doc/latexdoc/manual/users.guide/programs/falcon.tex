% $Revision: 8.1.14-03-31-20-gdcdffbb Patch(8.1.14-03-31-20-gdcdffbb): $
\section{\program{falcon}}
\label{UG:sec:falcon}
\index{Program!Falcon@\program{Falcon}}\index{Falcon@\program{Falcon}}

\subsection{Description}
\label{UG:sec:falcon_description}
%%%<MODULE NAME="FALCON">
%%Description:
%%%<HELP>
%%+FALCON calculates total energy and orbitals of the large system
%%+based on the fragment method.
%%%</HELP>
\program{FALCON} calculates total energy of the large system based on
the fragment approach.
Total energy of the whole system is calculated from total energies of
fragments as follows,
\[
   E^{whole}=\sum C_i^{fragment} E_i^{fragment},
\]
where $E_i^{fragment}$ is the total energy of fragment i, and
$C_i^{fragment}$ is its coefficient.

In addition to the total energy, \program{FALCON} can calculate orbitals
of the whole system.
Fock matrix and overlap matrix of the whole system are calculated from
ones of fragments using following equations,
\[
   F^{whole}=\sum C_i^{fragment} \textbf{F}_i^{fragment},
\]
and
\[
   S^{whole}=\sum C_i^{fragment} \textbf{S}_i^{fragment},
\]
where
$\textbf{F}_i$ and $\textbf{S}_i$ are the Fock matrix and overlap matrix,
respectively, of fragment i.

Then
\[
   \textbf{FC}=\textbf{SC$\varepsilon$}
\]
is solved to obtain the orbitals, \textbf{C}, and orbitals energies,
\textbf{$\varepsilon$}.


\subsection{Input}
\label{UG:falcon:falcon_inpfalcon}
\index{Input!FALCON}\index{FALCON!Input}

Below follows a description of the input to \program{FALCON}.

The input for each module is preceded by its name like:
\begin{inputlisting}
 &FALCON
\end{inputlisting}

Argument(s) to a keyword, either individual or composed by several entries,
can be placed in a separated line or in the same line separated by a semicolon.
If in the same line, the first argument requires an equal sign after the
name of the keyword.

\subsubsection{Keywords}
\begin{keywordlist}
%---
%%%<KEYWORD MODULE="FALCON" NAME="TITLE" KIND="STRING" LEVEL="BASIC">
%%Keyword: Title <basic>
%%%<HELP>
%%+One line title.
%%%</HELP></KEYWORD>
\item[TITLe]
One-line title.
%---
%%%<KEYWORD MODULE="FALCON" NAME="FRAGMENT" KIND="INT" LEVEL="BASIC">
%%Keyword: Fragment <basic> GUI:keyword
%%%<HELP>
%%+Takes one, two or three argument(s).
%%+The first value defines the fragment number, the second value determines coefficient,
%%+and the third value is the fragment number that is equivalent to this fragment
%%+when translational symmetry is used.
%%+Other keyword(s) specific to this fragment must follow this keyword.
%%%</HELP></KEYWORD>
\item[FRAGment]
Takes one, two or three argument(s).
The first value (integer) defines the fragment number,
the second value (real) determines coefficient,
and the third value (integer) is the fragment number that is equivalent
to this fragment when translational symmetry is used.
A default for the second value is 1.0 where the first and third values have
no default.
Other keyword(s) specific to this fragment must follow this keyword.
%---
%%%<KEYWORD MODULE="FALCON" NAME="OPERATOR" KIND="REAL" LEVEL="BASIC">
%%Keyword: Operator <basic> GUI:keyword
%%%<HELP>
%%+An integer following this keyword specifies the number of fragments.
%%%</HELP></KEYWORD>
\item[OPERator]
A real value following this keyword represents a coefficient, $C_i^{fragment}$,
of fragment i (current fragment), where i is a value specified by FRAGMENT keyword.
This keyword is equivalent with the second value of keyword, FRAGMENT.
%---
%%%<KEYWORD MODULE="FALCON" NAME="EQUIVALENCE" KIND="INT" LEVEL="BASIC">
%%Keyword: Equivalence <basic> GUI:keyword
%%%<HELP>
%%+An integer following this keyword specifies the number of fragments.
%%%</HELP></KEYWORD>
\item[EQUIvalence]
An integer, j, following this keyword declares that current fragment
is translationally equivalent with fragment j, and information provided for
fragment j are tranfered to current fragment.
This keyword is equivalent with the third value of keyword, FRAGMENT.
%---
%%%<KEYWORD MODULE="FALCON" NAME="TRANSLATE" KIND="REALs" SIZE="3" LEVEL="BASIC">
%%Keyword: Translate <basic> GUI:keyword
%%%<HELP>
%%+An integer following this keyword specifies the number of fragments.
%%%</HELP></KEYWORD>
\item[TRANslate]
Three real numbers following this keyword specifies the translational
vector by which the current fragment is translated to give new coordinate.
A unit of either bohr or angstrom can follow.  The default unit is angstrom.
This keyword takes effect only when the equivalent fragment is specified.
%---
%%%<KEYWORD MODULE="FALCON" NAME="RUNFILE" KIND="STRING" LEVEL="BASIC">
%%Keyword: RunFile <basic> GUI:keyword
%%%<HELP>
%%+Following this keyword specifies the name of RunFile file for the
%%+corresponding fragment.
%%%</HELP></KEYWORD>
\item[RUNFile]
Following this keyword specifies the name of RunFile file for the
corresponding fragment.
%---
%%%<KEYWORD MODULE="FALCON" NAME="ONEINT" KIND="STRING" LEVEL="BASIC">
%%Keyword: OneInt <basic> GUI:keyword
%%%<HELP>
%%+Following this keyword specifies the name of OneInt file for the
%%+corresponding fragment.
%%%</HELP></KEYWORD>
\item[ONEInt]
Following this keyword specifies the name of OneInt file for the
corresponding fragment.
%---
%%%<KEYWORD MODULE="FALCON" NAME="NFRAGMENT" KIND="INT" LEVEL="BASIC">
%%Keyword: nFragment <basic> GUI:keyword
%%%<HELP>
%%+An integer following this keyword specifies the number of fragments.
%%%</HELP></KEYWORD>
\item[NFRAgment]
An integer following this keyword specifies the number of fragments.
If this keyword is not given, the largest fragment number given by
FRAGMENT keyword is set to be the number of fragment.
%---
%%%<KEYWORD MODULE="FALCON" NAME="NIRREP" KIND="INT" LEVEL="BASIC">
%%Keyword: nIrrep <basic> GUI:keyword
%%%<HELP>
%%+An integer following this keyword specifies the number of irreducible
%%+representation of point group symmetry.
%%%</HELP></KEYWORD>
\item[NIRRep]
An integer following this keyword specifies the number of irreducible
representation of point group symmetry.
%---
%%%<KEYWORD MODULE="FALCON" NAME="OCCUPATION" KIND="INTS_LOOKUP" SIZE="NSYM" LEVEL="BASIC">
%%Keyword: Occupation <basic> GUI:keyword
%%%<HELP>
%%+A list of integer(s) following this keyword specifies the number of
%%+occupied orbitals in each symmetry representation.
%%%</HELP></KEYWORD>
\item[OCCUpation]
A list of integer(s) following this keyword specifies the number of
occupied orbitals in each symmetry representation in the unfragmented
system.
%---
%%%<KEYWORD MODULE="FALCON" NAME="DISTANCE" KIND="REAL" LEVEL="BASIC">
%%Keyword: Distance <basic> GUI:keyword
%%%<HELP>
%%+A real number following this keyword specifies the distance
%%+of two atoms that are equivalent to each other.
%%%</HELP></KEYWORD>
\item[DISTance]
A real number following this keyword specifies the distance
of two atoms that are equivalent to each other, followed by a unit that
is eather angstrom or bohr.
Default is angstrom.
%---
%%%<KEYWORD MODULE="FALCON" NAME="NEAR" KIND="REAL" LEVEL="BASIC">
%%Keyword: Near <basic> GUI:keyword
%%%<HELP>
%%+A real number following this keyword specifies the distance
%%+of two atoms within which atoms are considered to be too close each other.
%%%</HELP></KEYWORD>
\item[NEAR]
A real number following this keyword specifies the distance
of two atoms within which atoms are considered to be too close each other.
An unit that is eather angstrom or bohr can follow.
Default is angstrom.
%---
%%%<KEYWORD MODULE="FALCON" NAME="PRINT" KIND="INT" LEVEL="BASIC">
%%Keyword: Print <basic> GUI:keyword
%%%<HELP>
%%+An integer following this keyword specifies the format of orbital
%%+print out.
%%%</HELP></KEYWORD>
\item[PRINt]
An integer following this keyword specifies the format of orbital print out.
%---
%%%<KEYWORD MODULE="FALCON" NAME="ORBENE" KIND="REAL" LEVEL="BASIC">
%%Keyword: OrbEne <basic> GUI:keyword
%%%<HELP>
%%+An integer following this keyword specifies the format of orbital
%%+print out.
%%%</HELP></KEYWORD>
\item[ORBEne]
A real number follwing this keyword stands for the threshold for orbital print
out.
The orbitals with orbital energy below this value are print out.
%---
%%%<KEYWORD MODULE="FALCON" NAME="ORBOCC" KIND="REAL" LEVEL="BASIC">
%%Keyword: OrbOcc <basic> GUI:keyword
%%%<HELP>
%%+An integer following this keyword specifies the format of orbital
%%+print out.
%%%</HELP></KEYWORD>
\item[ORBOcc]
A real number follwing this keyword stands for the threshold for orbital print
out.
The orbitals with occupation number above this value are print out.
\end{keywordlist}

\subsubsection{Input examples}

Below shows an example of input file for the three fragment system of which
energy, $E^{whole}$, is written as
\[
   E^{whole}= E_1^{fragment} + E_2^{fragment} - E_3^{fragment},
\]
by fragment energies, $E_1^{fragment}$, $E_2^{fragment}$, and $E_3^{fragment}$.

\begin{inputlisting}
 &FALCON
 Fragment=1,  1.0
 Fragment=2,  1.0
 Fragment=3, -1.0
\end{inputlisting}

which can be simplified as,

\begin{inputlisting}
 &FALCON
 Fragment=3, -1.0
\end{inputlisting}

The next example is a two fragment system in which fragment 1 and fragment 2
are equivalent except for their positons.
When their difference in position is described by a vector, (1.0, 1.0, -1.0),
a translational symmetry can be used and the input becomes as follows,

\begin{inputlisting}
 &FALCON
 Fragment=2, 1.0, 1
 Translate=1.0, 1.0, -1.0
\end{inputlisting}

If the total energy of the whole system is given by the sum of total energies
of three fragment,
\[
   E^{whole}= E_1^{fragment} + E_2^{fragment} + E_3^{fragment},
\]
input is simplly as follows,

\begin{inputlisting}
 &FALCON
 nFragment=3
\end{inputlisting}
%%%</MODULE>
